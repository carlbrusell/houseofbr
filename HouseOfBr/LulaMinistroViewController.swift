//
//  FirstViewController.swift
//  HouseOfBr
//
//  Created by Carl Osorio on 3/19/16.
//  Copyright © 2016 Carl Osorio. All rights reserved.
//

import UIKit
import Parse
import GoogleMobileAds

class LulaMinistroViewController: UIViewController {

    @IBOutlet weak var statusFont: UILabel!
    @IBOutlet weak var statusText: UILabel!
    @IBOutlet weak var statusImage: UIImageView!
    
    @IBOutlet weak var banner: GADBannerView!
    
    let objs = ParseManager.sharedInstance.lula
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        banner.adUnitID = "ca-app-pub-3256688265604928/6180319111"
        banner.rootViewController = self
        banner.loadRequest(GADRequest())
        
        let obj = objs.lastObject as! PFObject
        
        self.eMinistro(obj)
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func eMinistro(status: PFObject){
        
        statusFont.text = status["fontName"] as? String
        
        if !(status["EMinistro"] as! Bool) {
            
            statusImage.image = UIImage(named: "LulaNao")
            statusText.text = "NÃO"
            
        }else{
            
            statusImage.image = UIImage(named: "LulaSim")
            statusText.text = "SIM"
            
        }
        
        
    }
    
    override func preferredStatusBarStyle() -> UIStatusBarStyle {
        
        return UIStatusBarStyle.LightContent
        
    }


}

