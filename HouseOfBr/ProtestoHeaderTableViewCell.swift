//
//  ProtestoHeaderTableViewCell.swift
//  HouseOfBr
//
//  Created by Carl Osorio on 3/20/16.
//  Copyright © 2016 Carl Osorio. All rights reserved.
//

import UIKit

class ProtestoHeaderTableViewCell: UITableViewCell {

    @IBOutlet weak var name: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
