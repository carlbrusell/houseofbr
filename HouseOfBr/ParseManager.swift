//
//  ParseManager.swift
//  HouseOfBr
//
//  Created by Carl Osorio on 3/20/16.
//  Copyright © 2016 Carl Osorio. All rights reserved.
//

import Parse

class ParseManager {
	
    var impeachment = NSMutableArray()
    var lula = NSMutableArray()
    var protestosPast = NSMutableArray()
    var protestosFuture = NSMutableArray()
    
    class var sharedInstance: ParseManager {
        struct Singleton {
            static let instance = ParseManager()
        }
        return Singleton.instance
    }
    
    func downloadEverything(){
        
        do {
            
            let queryImpeachment = PFQuery(className: "Impeachment")
            let objsImp = try queryImpeachment.findObjects()
            
            self.impeachment.addObjectsFromArray(objsImp)
            
            let queryMinistro = PFQuery(className: "Lula")
            let objsLula = try queryMinistro.findObjects()
            
            self.lula.addObjectsFromArray(objsLula)
            
            
            let queryProtestos = PFQuery(className: "Protesto")
            let objsProtestos = try queryProtestos.findObjects()
            
            for objP in objsProtestos {
                
                let data = objP["data"] as! NSDate
                
                if data.isGreaterThanDate(NSDate()) {
                    self.protestosFuture.addObject(objP)
                }else{
                    self.protestosPast.addObject(objP)
                }
                
            }
            
        }catch{
            
            print("DEU RUIM")
            
        }
        
    }
	
}

extension NSDate {
    func isGreaterThanDate(dateToCompare: NSDate) -> Bool {
        //Declare Variables
        var isGreater = false
        
        //Compare Values
        if self.compare(dateToCompare) == NSComparisonResult.OrderedDescending {
            isGreater = true
        }
        
        //Return Result
        return isGreater
    }
    
    func isLessThanDate(dateToCompare: NSDate) -> Bool {
        //Declare Variables
        var isLess = false
        
        //Compare Values
        if self.compare(dateToCompare) == NSComparisonResult.OrderedAscending {
            isLess = true
        }
        
        //Return Result
        return isLess
    }
    
    func equalToDate(dateToCompare: NSDate) -> Bool {
        //Declare Variables
        var isEqualTo = false
        
        //Compare Values
        if self.compare(dateToCompare) == NSComparisonResult.OrderedSame {
            isEqualTo = true
        }
        
        //Return Result
        return isEqualTo
    }
    
    func addDays(daysToAdd: Int) -> NSDate {
        let secondsInDays: NSTimeInterval = Double(daysToAdd) * 60 * 60 * 24
        let dateWithDaysAdded: NSDate = self.dateByAddingTimeInterval(secondsInDays)
        
        //Return Result
        return dateWithDaysAdded
    }
    
    func addHours(hoursToAdd: Int) -> NSDate {
        let secondsInHours: NSTimeInterval = Double(hoursToAdd) * 60 * 60
        let dateWithHoursAdded: NSDate = self.dateByAddingTimeInterval(secondsInHours)
        
        //Return Result
        return dateWithHoursAdded
    }
}
