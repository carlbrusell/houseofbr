//
//  ProtestoViewController.swift
//  HouseOfBr
//
//  Created by Carl Osorio on 3/20/16.
//  Copyright © 2016 Carl Osorio. All rights reserved.
//

import UIKit
import QuartzCore
import Parse
import GoogleMobileAds

class ProtestoViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var banner: GADBannerView!
    @IBOutlet weak var tableView: UITableView!
    let objsPast = ParseManager.sharedInstance.protestosPast
    let objsFuture = ParseManager.sharedInstance.protestosFuture
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        banner.adUnitID = "ca-app-pub-3256688265604928/6180319111"
        banner.rootViewController = self
        banner.loadRequest(GADRequest())
        
        self.tableView.registerNib(UINib(nibName: "ProtestoTableViewCell", bundle: nil), forCellReuseIdentifier: "protesto")
        
        self.tableView.registerNib(UINib(nibName: "ProtestoHeaderTableViewCell", bundle: nil), forCellReuseIdentifier: "protestoHeader")

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCellWithIdentifier("protesto", forIndexPath: indexPath) as! ProtestoTableViewCell
        
        cell.backgroundColor = UIColor.clearColor()
        
        let color : UIColor!
        
        let obj : PFObject!
        
        if indexPath.section == 0 {
            obj = self.objsFuture.objectAtIndex(indexPath.row) as! PFObject
        }else{
            obj = self.objsPast.objectAtIndex(indexPath.row) as! PFObject
        }
        
        if (obj["Lado"] as! Bool) {
            color = UIColor(red: 65/255, green: 117/255, blue: 5/255, alpha: 1)
        }else{
            color = UIColor(red: 208/255, green: 2/255, blue: 27/255, alpha: 1)
        }
        
        cell.name.text = obj["name"] as? String
        cell.place.text = obj["place"] as? String
        
        if obj["data"] != nil {
            
            let date = obj["data"] as! NSDate
            
            
            let formatter = NSDateFormatter()
            formatter.dateFormat = "dd\nMMM"
            
            cell.date.text = formatter.stringFromDate(date).uppercaseString
            
            let att = NSMutableAttributedString(string: formatter.stringFromDate(date).uppercaseString)
            att.addAttributes([NSFontAttributeName:UIFont.systemFontOfSize(24)], range: NSMakeRange(0, 2))
            att.addAttributes([NSFontAttributeName:UIFont.systemFontOfSize(14)], range: NSMakeRange(3, 3))
            
            cell.date.attributedText = att

        }else{

            cell.date.text = ""

        }
        
        if indexPath.section == 0 {
                
            cell.name.textColor = color
            cell.place.textColor = color
            cell.date.backgroundColor = color
                
        }else{
            
            cell.name.alpha = 0.5
            cell.name.textColor = color
            cell.place.alpha = 0.5
            cell.place.textColor = color
            cell.date.backgroundColor = UIColor.clearColor()
            cell.date.layer.borderColor = color.CGColor
            cell.date.layer.borderWidth = 1
            cell.date.alpha = 0.5
            cell.date.textColor = color
            
        }

        return cell
    }
    
    func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let header = self.tableView.dequeueReusableCellWithIdentifier("protestoHeader") as! ProtestoHeaderTableViewCell
        
        header.backgroundColor = UIColor.clearColor()
        
        if section % 2 == 0 {
            header.name.text = "Próximos protestos".uppercaseString
        }else{
            header.name.text = "O que já aconteceu".uppercaseString
            header.name.alpha = 0.5
        }

        
        return header
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 74;
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return self.objsFuture.count
        }else{
            return self.objsPast.count
        }
    }

}
