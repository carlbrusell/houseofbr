//
//  PassoTableViewCell.swift
//  HouseOfBr
//
//  Created by Carl Osorio on 3/20/16.
//  Copyright © 2016 Carl Osorio. All rights reserved.
//

import UIKit

class PassoTableViewCell: UITableViewCell {

    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var flag: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    func setHappened(status: Bool){
        
        if status {
            
            flag.backgroundColor = UIColor(red: 68/255.0, green: 219/255.0, blue: 94/255, alpha: 1)
            flag.layer.borderColor = UIColor(red: 68/255.0, green: 219/255.0, blue: 94/255, alpha: 1).CGColor
            flag.layer.borderWidth = 1
            
        }else{
            
            flag.backgroundColor = UIColor.clearColor()
            flag.layer.borderColor = UIColor.whiteColor().CGColor
            flag.layer.borderWidth = 1
            
        }
        
    }
    
}
