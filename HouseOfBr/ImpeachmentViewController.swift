//
//  SecondViewController.swift
//  HouseOfBr
//
//  Created by Carl Osorio on 3/19/16.
//  Copyright © 2016 Carl Osorio. All rights reserved.
//

import UIKit
import Parse
import GoogleMobileAds

class ImpeachmentViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var banner: GADBannerView!
    @IBOutlet weak var tableView: UITableView!
    
    let objs = ParseManager.sharedInstance.impeachment
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        banner.adUnitID = "ca-app-pub-3256688265604928/6180319111"
        banner.rootViewController = self
        banner.loadRequest(GADRequest())
        
        self.tableView.registerNib(UINib(nibName: "PassoTableViewCell", bundle: nil), forCellReuseIdentifier: "passo")
        self.tableView.rowHeight = UITableViewAutomaticDimension
        self.tableView.estimatedRowHeight = 160.0
        
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.objs.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCellWithIdentifier("passo", forIndexPath: indexPath) as! PassoTableViewCell

        let obj = objs.objectAtIndex(indexPath.row) as! PFObject
        
        cell.backgroundColor = UIColor.clearColor()
        cell.name?.text = obj["name"] as? String
        cell.setHappened(obj["itHappened"] as! Bool)
        
        return cell;
        
    }
    
    override func preferredStatusBarStyle() -> UIStatusBarStyle {
        
        return UIStatusBarStyle.LightContent
        
    }


}

